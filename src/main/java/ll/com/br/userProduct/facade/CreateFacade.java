package ll.com.br.userProduct.facade;

import ll.com.br.userProduct.model.User;
import ll.com.br.userProduct.model.UserProduct;
import ll.com.br.userProduct.service.UserProductService;
import ll.com.br.userProduct.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateFacade {

    @Autowired
    private UserService userService;

    @Autowired
    private UserProductService userProductService;

    public User saveUser(User user){
        return userService.saveUser(user);
    }

    public UserProduct saveUserProduct(Long userId, Long productId) {
        return userProductService.saveUserProduct(userId, productId);
    }
}
