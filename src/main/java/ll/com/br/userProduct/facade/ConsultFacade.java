package ll.com.br.userProduct.facade;

import ll.com.br.userProduct.model.Product;
import ll.com.br.userProduct.model.User;
import ll.com.br.userProduct.service.ProductService;
import ll.com.br.userProduct.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ConsultFacade {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    public User getUserById(Long id) {
        return userService.getUserById(id);
    }

    public List<Product> getAllProduts(){
        return productService.getAllProduts();
    }

    public Optional<Product> getProductById(Long id) {
        return productService.getProductById(id);
    }
}
