package ll.com.br.userProduct.facade;

import ll.com.br.userProduct.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteFacade {

    @Autowired
    private UserService userService;

    public void deleteUser(Long id) {
        userService.deleteUser(id);
    }
}
