package ll.com.br.userProduct.facade;

import ll.com.br.userProduct.model.User;
import ll.com.br.userProduct.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateFacade {

    @Autowired
    private UserService userService;

    public User updateUser(User user) {
        return userService.updateUser(user);
    }
}
