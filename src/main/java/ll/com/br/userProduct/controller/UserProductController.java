package ll.com.br.userProduct.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ll.com.br.userProduct.facade.CreateFacade;
import ll.com.br.userProduct.model.UserProduct;
import ll.com.br.userProduct.service.UserProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/userProduct")
@Api(value = "Endpoints que vinculua usuário ao produto")
public class UserProductController {

    @Autowired
    private CreateFacade createFacade;

    @ApiOperation(value = "Endpoint responsavel por vincular um usuário a um produto")
    @PostMapping
    public @ResponseBody UserProduct saveUserProduct(@RequestBody UserProduct userProduct){
        return createFacade.saveUserProduct(userProduct.getUser().getId(), userProduct.getProduct().getId());
    }
}
