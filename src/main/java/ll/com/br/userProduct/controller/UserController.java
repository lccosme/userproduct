package ll.com.br.userProduct.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ll.com.br.userProduct.facade.ConsultFacade;
import ll.com.br.userProduct.facade.CreateFacade;
import ll.com.br.userProduct.facade.DeleteFacade;
import ll.com.br.userProduct.facade.UpdateFacade;
import ll.com.br.userProduct.model.User;
import ll.com.br.userProduct.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Api(value = "Endpoints usuário")
public class UserController {

    @Autowired
    private CreateFacade createFacade;

    @Autowired
    private UpdateFacade updateFacade;

    @Autowired
    private ConsultFacade consultFacade;

    @Autowired
    private DeleteFacade deleteFacade;

    @ApiOperation(value = "Endpoint responsavel por cadastro de usuário")
    @PostMapping
    public @ResponseBody User saveUser(@RequestBody User user){
        return createFacade.saveUser(user);
    }

    @ApiOperation(value = "Endpoint responsavel por consulta de usuário por id")
    @GetMapping("/id/{userId}")
    public @ResponseBody User getUser(@PathVariable("userId") Long userId){
        return consultFacade.getUserById(userId);
    }

    @ApiOperation(value = "Endpoint responsavel por deletar usuário por id")
    @DeleteMapping("/id/{userId}")
    public void deleteUser(@PathVariable("userId") Long userId){
        deleteFacade.deleteUser(userId);
    }

    @ApiOperation(value = "Endpoint responsavel por atualizar usuário por id")
    @PutMapping()
    public @ResponseBody User updateUser(@RequestBody() User user){
        return updateFacade.updateUser(user);
    }
}
