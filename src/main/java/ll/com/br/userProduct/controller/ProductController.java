package ll.com.br.userProduct.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ll.com.br.userProduct.facade.ConsultFacade;
import ll.com.br.userProduct.model.Product;
import ll.com.br.userProduct.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
@Api(value = "Endpoints produtos")
public class ProductController {

    @Autowired
    private ConsultFacade consultFacade;

    @GetMapping
    @ApiOperation(value = "Endpoint responsavel por consultar todos os produtos cadastrados")
    public @ResponseBody List<Product> getProducList(){
        return consultFacade.getAllProduts();
    }
}
