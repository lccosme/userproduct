package ll.com.br.userProduct.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class MessageUtil {

    @Autowired
    private MessageSource messageSource;

    public String getMessage( String pMsg ){

        Locale locale = LocaleContextHolder.getLocale();

        return messageSource.getMessage(pMsg, null, locale);

    }
}
