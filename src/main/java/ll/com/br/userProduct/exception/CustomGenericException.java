package ll.com.br.userProduct.exception;

import org.springframework.http.HttpStatus;

public class CustomGenericException extends RuntimeException {

        private String errMsg;
        private HttpStatus statusCode;

    public CustomGenericException(String errMsg, HttpStatus statusCode) {
        super(errMsg);
        this.errMsg = errMsg;
        this.statusCode = statusCode;
    }
}
