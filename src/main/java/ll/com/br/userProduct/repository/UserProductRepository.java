package ll.com.br.userProduct.repository;

import ll.com.br.userProduct.model.User;
import ll.com.br.userProduct.model.UserProduct;
import ll.com.br.userProduct.model.enums.StatusUserProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserProductRepository extends CrudRepository<UserProduct, Long> {

    UserProduct findUserProductByUserAndStatusUserProduct(User user, StatusUserProduct statusUserProduct);

}
