package ll.com.br.userProduct.service;

import ll.com.br.userProduct.model.User;
import ll.com.br.userProduct.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    public User updateUser(User user) {
        User userFound = Optional.ofNullable(userRepository.findById(user.getId()))
                .orElseThrow(() -> new IllegalArgumentException("NAO_FOI_ENCONTRADO_DADOS_CADASTRADADOS_COM_AS_INFORMACAES_FORNECIDAS")).get();

        userFound.setName(user.getName());
        userFound.setAddress(user.getAddress());
        userFound.setDocumentNumber(user.getDocumentNumber());
        userFound.setEmail(user.getEmail());
        userFound.setPhone(user.getPhone());
        return userRepository.save(userFound);
    }

    public User getUserById(Long id) {
        return Optional.ofNullable(userRepository.findById(id))
                .orElseThrow(() -> new IllegalArgumentException("NAO_FOI_ENCONTRADO_DADOS_CADASTRADADOS_COM_AS_INFORMACAES_FORNECIDAS")).get();
    }
}
