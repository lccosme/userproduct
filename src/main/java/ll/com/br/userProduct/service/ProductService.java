package ll.com.br.userProduct.service;

import ll.com.br.userProduct.model.Product;
import ll.com.br.userProduct.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProduts(){
        return (List<Product>) productRepository.findAll();
    }

    public Optional<Product> getProductById(Long id) {
        return Optional.ofNullable(productRepository.findById(id))
                .orElseThrow(() -> new IllegalArgumentException("NAO_FOI_ENCONTRADO_DADOS_CADASTRADADOS_COM_AS_INFORMACAES_FORNECIDAS"));
    }
}
