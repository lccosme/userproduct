package ll.com.br.userProduct.service;

import ll.com.br.userProduct.exception.CustomGenericException;
import ll.com.br.userProduct.model.Product;
import ll.com.br.userProduct.model.User;
import ll.com.br.userProduct.model.UserProduct;
import ll.com.br.userProduct.model.enums.StatusUserProduct;
import ll.com.br.userProduct.repository.UserProductRepository;
import ll.com.br.userProduct.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserProductService {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Autowired
    private UserProductRepository userProductRepository;

    @Autowired
    private MessageUtil messageUtil;

    public UserProduct saveUserProduct(Long userId, Long productId) {
        try {
            User userCurrent = userService.getUserById(userId);
            Product product = productService.getProductById(productId).get();

            UserProduct userProductFound = findUserProduct(userCurrent);

            if (Objects.nonNull(userProductFound)) {
                updateUserProduct(userCurrent, userProductFound);
            }

            UserProduct userProduct = createUserProduct(userCurrent, product);
            return userProductRepository.save(userProduct);
        } catch (Exception e) {
            throw new CustomGenericException(messageUtil.getMessage("Erro ao vincular usuario ao produto") + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private UserProduct findUserProduct(User userCurrent) {
        return userProductRepository
                .findUserProductByUserAndStatusUserProduct(userCurrent, StatusUserProduct.VALIDO);
    }

    protected UserProduct createUserProduct(User user, Product product) {
        UserProduct userProduct = new UserProduct();
        userProduct.setUser(user);
        userProduct.setProduct(product);
        userProduct.setStatusUserProduct(StatusUserProduct.VALIDO);
        return userProduct;
    }

    protected void updateUserProduct(User user, UserProduct userProduct) {
        if (Objects.nonNull(userProduct.getId())) {
            userProduct.setStatusUserProduct(StatusUserProduct.INVALIDO);
            userProductRepository.save(userProduct);
        }
    }
}
