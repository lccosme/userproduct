package ll.com.br.userProduct.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column( nullable = false )
    private String name;

    @NotNull
    @Column( nullable = false )
    private String address;

    @Column
    private String phone;

    @NotNull
    @Column( nullable = false )
    private String email;

    @NotNull
    @Column( unique = true, nullable = false )
    private String documentNumber;
}
