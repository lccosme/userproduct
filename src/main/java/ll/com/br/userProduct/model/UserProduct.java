package ll.com.br.userProduct.model;

import ll.com.br.userProduct.model.enums.StatusUserProduct;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter

@Entity
@Table(name = "user_product")
public class UserProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Enumerated(value = EnumType.STRING)
    private StatusUserProduct statusUserProduct;
}
