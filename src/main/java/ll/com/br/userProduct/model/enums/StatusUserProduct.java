package ll.com.br.userProduct.model.enums;

public enum StatusUserProduct {

    VALIDO(1, "valido"),
    INVALIDO(2, "invalido");

    private Integer key;
    private String value;

    StatusUserProduct(Integer key, String value){
        this.key = key;
        this.value = value;
    }
}
