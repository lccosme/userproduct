package ll.com.br.userProduct.service;

import ll.com.br.userProduct.model.User;
import ll.com.br.userProduct.repository.UserRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    @Spy
    private UserService userService;

    /*

    OBSERVAÇÃO: DEVIDO AO POUCO TEMPO NÃO FOI POSSIVEL IMPLEMENTAR MAIS TESTES UNITÁRIOS.

    */

    @Test
    public void saveUser_Test(){
        User user = new User();
        user.setName("lucas");

        doReturn(user).when(userRepository).save(any(User.class));

        User userResult = userService.saveUser(user);

        Assert.assertNotNull(userResult);
        Assert.assertEquals("lucas", userResult.getName());
    }

}